package com.yxj.gulimall.product.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.yxj.gulimall.common.utils.PageUtils;
import com.yxj.gulimall.common.utils.Query;
import com.yxj.gulimall.product.service.CategoryBrandRelationService;
import com.yxj.gulimall.product.vo.Catelog2Vo;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yxj.gulimall.product.dao.CategoryDao;
import com.yxj.gulimall.product.entity.CategoryEntity;
import com.yxj.gulimall.product.service.CategoryService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

/**
 * 商品三级分类
 *
 * @author yaoxinjia
 */
@Service("categoryService")
public class CategoryServiceImpl extends ServiceImpl<CategoryDao, CategoryEntity> implements CategoryService {


    @Autowired
    CategoryBrandRelationService categoryBrandRelationService;

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Autowired
    RedissonClient redisson;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CategoryEntity> page = this.page(
                new Query<CategoryEntity>().getPage(params),
                new QueryWrapper<CategoryEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<CategoryEntity> listWithTree() {
        // 1 查出所有分类
        List<CategoryEntity> entities = baseMapper.selectList(null);
        // 2 组装成父子的树形结构
        List<CategoryEntity> level1Menus = entities.stream().filter(categoryEntity ->
                categoryEntity.getParentCid().longValue() == 0
        ).map((menu) -> {
            menu.setChildren(getChildrens(menu, entities));
            return menu;
        }).sorted((menu1, menu2) -> {
            return (menu1.getSort() == null ? 0 : menu1.getSort()) - (menu2.getSort() == null ? 0 : menu2.getSort());
        }).collect(Collectors.toList());
        return level1Menus;
    }


    /**
     * 递归查找所有菜单的子菜单
     *
     * @param root
     * @param all
     * @return
     */
    private List<CategoryEntity> getChildrens(CategoryEntity root, List<CategoryEntity> all) {
        List<CategoryEntity> children = all.stream().filter(categoryEntity -> {
            return categoryEntity.getParentCid().longValue() == root.getCatId().longValue();  // 注意此处应该用longValue()来比较，否则会出先bug，因为parentCid和catId是long类型
        }).map(categoryEntity -> {
            // 1 找到子菜单
            categoryEntity.setChildren(getChildrens(categoryEntity, all));
            return categoryEntity;
        }).sorted((menu1, menu2) -> {
            // 2 菜单的排序
            return (menu1.getSort() == null ? 0 : menu1.getSort()) - (menu2.getSort() == null ? 0 : menu2.getSort());
        }).collect(Collectors.toList());
        return children;
    }

    @Override
    public void removeMenuByIds(List<Long> asList) {
        // TODO 1 检查当前删除的菜单，是否被别的地方引用

        // 逻辑删除
        baseMapper.deleteBatchIds(asList);
    }

    @Override
    public Long[] findCatelogPath(Long catelogId) {
        List<Long> paths = new ArrayList<>();
        List<Long> parentPath = findParentPath(catelogId, paths);

        Collections.reverse(parentPath);//集合元素反转
        return parentPath.toArray(new Long[parentPath.size()]);
    }

    /**
     * 级联更新所有关联的数据
     *           - 加锁保证并发读写，读写锁
     * @param category
     * Spring Cache就是一个这样的框架。它利用了AOP，实现了基于注解的缓存功能，并且进行了合理的抽象，
     * 业务代码不用关心底层是使用了什么缓存框架，只需要简单地加一个注解，就能实现缓存功能了。
     * 而且Spring Cache也提供了很多默认的配置，用户可以3秒钟就使用上一个很不错的缓存功能。
     *                                    - 基于注解的缓存功能
     */
    @CacheEvict(value = {"category"},key = "#root.method.name", allEntries = true)  // 失效模式
    @CachePut // 双写模式
    @Transactional //事务保证双写一致性
    @Override
    public void updateCasecade(CategoryEntity category) {
        this.updateById(category);
        categoryBrandRelationService.updateCategory(category.getCatId(), category.getName());
    }

    // 每一个需要缓存的数据我们都要指定放到哪个名字的缓存。【缓存的分区（按照业务类型）】

    @Cacheable(value = {"category"}, key = "#root.method.name", sync = true) //多用于查询
    // 当前方法的结果需要缓存
    /**
     * 1）如果缓存中有，方法不调用，如果缓存中没有，会调用方法，最后将方法的结果放入缓存
     * 2）key默认自动生成：缓存的名字::SimpleKey（自主生成的key值）
     * 3）缓存的value值，默认使用jdk序列化机制，将序列化后的数据存到redis
     * 4）默认ttl时间 -1
     *
     *
     * 自定义：
     * 1）指定生成的缓存使用的key， /key属性指定，接受一个SpEL  https://docs.spring.io/spring-framework/reference/integration/cache/annotations.html
     * 2）指定缓存的数据存活的时间，/配置文件中修改ttl  spring.cache.redis.time-to-live=3600000
     * 3）将数据存为json格式
     */

    @Override
    public List<CategoryEntity> getLevel1Categorys() {
        long l = System.currentTimeMillis();
        List<CategoryEntity> categoryEntities = baseMapper.selectList(new QueryWrapper<CategoryEntity>().eq("parent_cid", 0));
        System.out.println("消耗时间，" + (System.currentTimeMillis() - 1));
        return categoryEntities;
    }

    //JSON跨语言、跨平台兼容/保存对象
    // JSON.toJSONString();将拿到的json数据逆转为能用的对象类型：【序列化与反序列化】

    /**
     * 1、同时进行多种缓存操作  @Caching(evict={a,b})
     * 2、指定删除某个分区下的所有数据 @CacheEvict(value = {"category"},key = "#root.method.name", allEntries = true)  // 失效模式
     * 3）存储同一类型的数据，都可以指定成同一个分区。分区名默认就是缓存的前缀  / 前缀不开启时，分区就相当于一个标识的作用
     * @return
     */
//    @Caching(put = {})
    @Cacheable(value = "category", key = "#root.methodName")
    @Override
    public Map<String, List<Catelog2Vo>> getCatalogJson() {
        System.out.println("查询了数据库......");
        List<CategoryEntity> selectList = baseMapper.selectList(null);
        System.out.println(selectList);
        // 查询所有一级分类
        List<CategoryEntity> level1Category = getParent_cid(selectList, 0L);

        // 2 封装数据
        Map<String, List<Catelog2Vo>> parent_cid = level1Category.stream().collect(Collectors.toMap(k -> k.getCatId().toString(), v -> {
                    // 1 每一个的一级分类，查到这个一级分类的二级分类
                    List<CategoryEntity> categoryEntities = getParent_cid(selectList, v.getCatId());
                    // 2 分装上面的结果
                    List<Catelog2Vo> catelog2Vos = null;

                    if (categoryEntities != null) {

                        catelog2Vos = categoryEntities.stream().map(l2 -> {
                            Catelog2Vo catelog2Vo = new Catelog2Vo(v.getCatId().toString(), null, l2.getCatId().toString(), l2.getName());
                            // 1 找当前二级分类的三级分类封装成vo
                            List<CategoryEntity> level3Catelog = getParent_cid(selectList, l2.getCatId());
                            if (level3Catelog != null) {
                                List<Catelog2Vo.Catelog3Vo> collect = level3Catelog.stream().map(l3 -> {
                                    // 2 分装成指定格式
                                    Catelog2Vo.Catelog3Vo catelog3Vo = new Catelog2Vo.Catelog3Vo(l2.getCatId().toString(), l3.getCatId().toString(), l3.getName());
                                    return catelog3Vo;
                                }).collect(Collectors.toList());
                                catelog2Vo.setCatalog3List(collect);
                            }
                            return catelog2Vo;
                        }).collect(Collectors.toList());
                    }
                    return catelog2Vos;

                }

        ));
        return parent_cid;
    }

    /**
     * 递归思想实现获取三级分类树形结构
     *
     * @return
     */
    @Override
    public List<CategoryEntity> listTree() {
        List<CategoryEntity> all = this.list(null);
        List<CategoryEntity> data = all.stream().filter(item -> {
            return item.getParentCid().longValue() == 0;//找到一级分类
        }).map(item -> {
            item.setChildren(getAllChilden(item, all));
            return item;
        }).collect(Collectors.toList());
        return data;
    }

    private List<CategoryEntity> getAllChilden(CategoryEntity entity, List<CategoryEntity> all) {
        List<CategoryEntity> collect = all.stream().filter(
                item -> {
                    return item.getParentCid().longValue() == entity.getCatId();
                }
        ).map(item -> {
            item.setChildren(getAllChilden(item, all));
            return item;
        }).collect(Collectors.toList());
        return collect;
    }

    // @Override
    public Map<String, List<Catelog2Vo>> getCatalogJson2() {
        // 给缓存中放json字符串，拿出的json字符串，还用逆转为能用的对象类型（序列化与反序列化）
        /**
         * 1 空结果缓存，并加入短暂的过期时间，解决缓存穿透-数据不存在
         * 2 设置过期时间（加随机值） ，解决缓存雪崩-集体失效
         * 3 加锁，解决缓存击穿-》一个线程获取锁，查询数据库并把数据放到缓存，其它线程再获取锁的时候直接去缓存中得到数据/预热数据设置-热点数据失效高并发访问
         */
        // 1 加入缓存逻辑，缓存中存的数据是json字符串
        // JSON跨语言
        String catalogJSON = redisTemplate.opsForValue().get("catalogJSON");
        if (StringUtils.isEmpty(catalogJSON)) {
            // 2 缓存中没有，查询数据库
            System.out.println("缓存不命中...将要查询数据库");
            Map<String, List<Catelog2Vo>> catalogJsonFromDb = getCatalogJsonFromDbWithRedislock();
            return catalogJsonFromDb;
        }
        System.out.println("缓存命中...直接返回"); //TypeReference 反序列化可以将json字符串指明想转化的对象类型
        Map<String, List<Catelog2Vo>> result = JSON.parseObject(catalogJSON, new TypeReference<Map<String, List<Catelog2Vo>>>() {
        });
        return result;
    }

    public Map<String, List<Catelog2Vo>> getCatalogJsonFromDbWithRedislock() {
        //在分布式情况下，想要锁住所有，必须使用分布式锁
        // 1 占分布式锁，去redis占坑-性能较比本地锁慢
        String uuid = UUID.randomUUID().toString();
        Boolean lock = redisTemplate.opsForValue().setIfAbsent("lock", uuid, 300, TimeUnit.SECONDS);//值的真假相当于上锁
        if (lock) {
            System.out.println("获取分布式锁成功");
            // 加锁成功...执行业务
            // 2 设置过期时间
//            redisTemplate.expire("lock", 30, TimeUnit.SECONDS);
            Map<String, List<Catelog2Vo>> dataFromDB;
            try {
                dataFromDB = getDataFromDB();

            } finally {                              //删除的key值，传过来的参数
                String script = "if redis.call('get', KEYS[1]) == ARGV[1] then return redis.call('del', KEYS[1]) else return 0 end";//lua脚本原子操作-一个操作不可分割，都成功，都失败
                // 删除锁
                Long lock1 = redisTemplate.execute(new DefaultRedisScript<Long>(script, Long.class), Arrays.asList("lock"), uuid);
            }
//            redisTemplate.delete("lock");
            // 获取值对比 + 对比成功删除 = 原子操作  lua脚本解锁
//            String lockValue = redisTemplate.opsForValue().get("lock");
//            if (uuid.equals(lockValue)) {
//                // 删除我自己的锁
//                redisTemplate.delete("lock");
//            }

            return dataFromDB;
        } else {
            // 加锁失败
            // 休眠100ms重试
            System.out.println("获取分布式锁失败...等待重试");
            try {
                Thread.sleep(200);
            } catch (Exception e) {

            }
            return getCatalogJsonFromDbWithRedislock(); // 自旋的方式
        }


    }

    /**
     * 缓存里面的数据如何和数据库保持一致
     * 缓存数据的一致性
     * 1）双写模式
     * 2）失效模式
     * @CacheEvict(value = "category", allEntries = true)  // 失效模式
     * @CachePut // 双写模式
     *
     * 方案：
     * 1、缓存的所有数据都有过期时间，数据过期下一次查询触发主动更新
     * 2、读写数据的时候，加上分布式的读写锁（经常读 经常写）
     *
     * @return
     */
    public Map<String, List<Catelog2Vo>> getCatalogJsonFromDbWithRedissonlock() {

        // 1 锁的名字。锁的粒度（划分），越细越快
        //锁的粒度：具体缓存的是某个数据
        RLock lock = redisson.getLock("CatalogJson-lock");
        lock.lock();//阻塞式等待
        Map<String, List<Catelog2Vo>> dataFromDB;
        try {
            dataFromDB = getDataFromDB();

        } finally {
            lock.unlock();
        }
        return dataFromDB;


    }

    private Map<String, List<Catelog2Vo>> getDataFromDB() {
        String catalogJSON = redisTemplate.opsForValue().get("catalogJSON");
        if (!StringUtils.isEmpty(catalogJSON)) {
            // 缓存不为null直接返回
            Map<String, List<Catelog2Vo>> result = JSON.parseObject(catalogJSON, new TypeReference<Map<String, List<Catelog2Vo>>>() {
            });
            return result;
        }
        System.out.println("查询了数据库......");

        List<CategoryEntity> selectList = baseMapper.selectList(null);
        System.out.println(selectList);
        // 查询所有一级分类
        List<CategoryEntity> level1Category = getParent_cid(selectList, 0L);

        // 2 封装数据
        Map<String, List<Catelog2Vo>> parent_cid = level1Category.stream().collect(Collectors.toMap(k -> k.getCatId().toString(), v -> {
                    // 1 每一个的一级分类，查到这个一级分类的二级分类
                    List<CategoryEntity> categoryEntities = getParent_cid(selectList, v.getCatId());
                    // 2 分装上面的结果
                    List<Catelog2Vo> catelog2Vos = null;

                    if (categoryEntities != null) {

                        catelog2Vos = categoryEntities.stream().map(l2 -> {
                            Catelog2Vo catelog2Vo = new Catelog2Vo(v.getCatId().toString(), null, l2.getCatId().toString(), l2.getName());
                            // 1 找当前二级分类的三级分类封装成vo
                            List<CategoryEntity> level3Catelog = getParent_cid(selectList, l2.getCatId());
                            if (level3Catelog != null) {
                                List<Catelog2Vo.Catelog3Vo> collect = level3Catelog.stream().map(l3 -> {
                                    // 2 分装成指定格式
                                    Catelog2Vo.Catelog3Vo catelog3Vo = new Catelog2Vo.Catelog3Vo(l2.getCatId().toString(), l3.getCatId().toString(), l3.getName());
                                    return catelog3Vo;
                                }).collect(Collectors.toList());
                                catelog2Vo.setCatalog3List(collect);
                            }
                            return catelog2Vo;
                        }).collect(Collectors.toList());
                    }
                    return catelog2Vos;

                }

        ));

        // 3 查到的数据放入缓存，将对象转为json放在缓存中
        String s = JSON.toJSONString(parent_cid);
        redisTemplate.opsForValue().set("catalogJSON", s, 1, TimeUnit.DAYS);//设置缓存的同时设置过期时间
        return parent_cid;
    }

    // 从数据库查询并封装分类数据
//    public synchronized Map<String, List<Catelog2Vo>> getCatalogJsonFromDbWithLocallock() {
    public Map<String, List<Catelog2Vo>> getCatalogJsonFromDbWithLocallock() {


        // 只要是同一把锁，就能锁住需要这个锁的所有线程
        //SpringBoot所有的组件在容器中都是单例的，高并发的时候还是只能调用同一个CategoryServiceImpl中的getCatalogJsonFromDbWithLocallock()方法，锁能锁住
        synchronized (this) {        //    同步方法-同步代码块

            // 得到锁之后，我们应该再去缓存中查询确定一次，如果没有才需要查询
            return getDataFromDB();

        }


    }

    private List<CategoryEntity> getParent_cid(List<CategoryEntity> selectList, Long parent_cid) {
        List<CategoryEntity> collect = selectList.stream().filter(item -> {
            return item.getParentCid() == parent_cid;
        }).collect(Collectors.toList());
        return collect;
        // return baseMapper.selectList(new QueryWrapper<CategoryEntity>().eq("parent_cid", v.getCatId()));
    }

    //225,25,2
    private List<Long> findParentPath(Long catelogId, List<Long> paths) {
        // 1 收集当前节点id
        paths.add(catelogId);
        CategoryEntity byId = this.getById(catelogId);
        if (byId.getParentCid() != 0) {
            findParentPath(byId.getParentCid(),paths);//递归思想
        }
        return paths;
        }
}