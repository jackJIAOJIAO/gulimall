package com.yxj.gulimall.order.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;



/**
 *************RabbitAutoConfiguration    rabbit自动加载配置类
 *  //申明一个绑定
 *          * public static enum DestinationType {
 *          *         QUEUE,
 *          *         EXCHANGE;
 *          *
 *          *         private DestinationType() {
 *          *         }
 *          *     }
 * ***********创建消息各组件，可以用AmqpAdmin
 *             Binding binding=new Binding(msgQueue,Binding.DestinationType.QUEUE,  //交换机绑定的目的地，目的地类型（可以是队列，也可以是交换机）
 *            msg.direct.exchange",msgKey,null);
 *             amqpAdmin.declareBinding(binding);
 *
 * 如果发送的消息是个对象，我们将会使用序列化机制，将对象写出去，对象必须实现Serializable（跨平台，跨语言接收）
 *
 * **********发送消息：使用RabbitTemplate
 *         rabbitTemplate.convertAndSend("msg.direct.exchange",key,message,correlationData);
 *
 * **********监听接收消息：使用   @     (queues) + @RabbitHandler          (必须加上@EnableRabbit)
 * @RabbitListener(queues = "order.release.order.queue")   +  @RabbitHandler//在对应的方法上执行具体接收操作
 *                    @RabbitListener标在 类+方法上   @RabbitHandler标在 方法上
 *
 ********* Queue 可以很多人来监听。只要收到消息，队列删除消息，而且只能有一个收到此消息
 *     场景：
 *         1）、订单服务启动多个；同一个消息（队列中），只能有一个客户端收到
 *         2）、只有一个消息完全处理完，方法运行结束，我们就可以接收到下一个消息
 *         3）、一个生产者生产消息，多个消费者同时收到同一个消息（发布-订阅）---topicExchange (条件广播) / FanoutExchange（无条件广播）
 *                                                                      （DirectExchange 单播-点对点 HeaderExchange）
 *
 * @author yaoxinjia
 * @email 894548575@qq.com
 */
@Configuration
public class MyRabbitMQConfig {

    /* 容器中的Queue、Exchange、Binding 会自动创建（在RabbitMQ）不存在的情况下 */

    /**
     * 死信队列
     *
     * @return
     */@Bean
    public Queue orderDelayQueue() {
        /*
            Queue(String name,  队列名字
            boolean durable,  是否持久化
            boolean exclusive,  是否排他，只能用于一个
            boolean autoDelete, 是否自动删除
            Map<String, Object> arguments) 属性
         */
        HashMap<String, Object> arguments = new HashMap<>();
        arguments.put("x-dead-letter-exchange", "order-event-exchange");
        arguments.put("x-dead-letter-routing-key", "order.release.order");
        arguments.put("x-message-ttl", 60000); // 消息过期时间 1分钟
        Queue queue = new Queue("order.delay.queue", true, false, false, arguments);

        return queue;
    }

    /**
     * 普通队列
     *
     * @return
     */
    @Bean
    public Queue orderReleaseQueue() {

        Queue queue = new Queue("order.release.order.queue", true, false, false);

        return queue;
    }

    /**
     * TopicExchange
     *
     * @return
     */
    @Bean
    public Exchange orderEventExchange() {
        /*
         *   String name,
         *   boolean durable,
         *   boolean autoDelete,
         *   Map<String, Object> arguments
         * */
        return new TopicExchange("order-event-exchange", true, false);

    }


    @Bean
    public Binding orderCreateBinding() {
        /*
         * String destination, 目的地（队列名或者交换机名字）
         * DestinationType destinationType, 目的地类型（Queue、Exhcange）
         * String exchange,
         * String routingKey,
         * Map<String, Object> arguments
         * */
        return new Binding("order.delay.queue",
                Binding.DestinationType.QUEUE,
                "order-event-exchange",
                "order.create.order",
                null);
    }

    @Bean
    public Binding orderReleaseBinding() {

        return new Binding("order.release.order.queue",
                Binding.DestinationType.QUEUE,
                "order-event-exchange",
                "order.release.order",
                null);
    }

    /**
     * 订单释放直接和库存释放进行绑定
     * @return
     */
    @Bean
    public Binding orderReleaseOtherBinding() {

        return new Binding("stock.release.stock.queue",
                Binding.DestinationType.QUEUE,
                "order-event-exchange",
                "order.release.other.#",
                null);
    }


    /**
     * 商品秒杀队列
     * @return
     */
    @Bean
    public Queue orderSecKillOrrderQueue() {
        Queue queue = new Queue("order.seckill.order.queue", true, false, false);
        return queue;
    }

    @Bean
    public Binding orderSecKillOrrderQueueBinding() {
        //String destination, DestinationType destinationType, String exchange, String routingKey,
        // 			Map<String, Object> arguments
        Binding binding = new Binding(
                "order.seckill.order.queue",
                Binding.DestinationType.QUEUE,
                "order-event-exchange",
                "order.seckill.order",
                null);

        return binding;
    }


}
