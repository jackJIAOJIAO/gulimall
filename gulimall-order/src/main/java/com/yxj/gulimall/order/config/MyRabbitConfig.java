package com.yxj.gulimall.order.config;

import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;


/**
 * @author yaoxinjia
 * @email 894548575@qq.com
 */
@Configuration
public class MyRabbitConfig {

    private RabbitTemplate rabbitTemplate;

    @Primary
    @Bean
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        this.rabbitTemplate = rabbitTemplate;
        rabbitTemplate.setMessageConverter(messageConverter());
        initRabbitTemplate();
        return rabbitTemplate;
    }

    /**
     * 消息转化器，将内容转换为json数据
     * private MessageConverter messageConverter = new SimpleMessageConverter();在RabbitTemplate中默认加载这个，
     * RabbitAutoConfiguration中如果容器中有，加载容器的，如果没有，加载默认的
     * @return
     */
    @Bean
    public MessageConverter messageConverter() {
        return new Jackson2JsonMessageConverter();//自定义消息转化器Bean，json格式
    }

    /**
     * 定制RabbitTemplate
     * 1、服务器收到消息就会回调   --->    生产者发送消息到达Broker后自动触发
     *      1、spring.rabbitmq.publisher-confirms: true
     *      2、设置确认回调：rabbitTemplate.setConfirmCallback(RabbitTemplate.ConfirmCallback)
     * 2、消息不正确抵达队列就会进行回调
     *      1、spring.rabbitmq.publisher-returns: true
     *         spring.rabbitmq.template.mandatory: true
     *      2、设置确认回调ReturnCallback
     *
     * 3、消费端确认(保证每个消息都被正确消费，此时才可以broker删除这个消息)
     *    # 手动ack消息，不使用默认的消费端确认-不自动回复导致消息丢失
     *    spring.rabbitmq.listener.simple.acknowledge-mode=manual
     *            1、默认是自动确认的，只要消息接收到，客户端会自动确认，服务端就会移除这个消息
     *                 问题：
     *                 我们收到很多消息，自动回复给服务器ack，只有一个消息处理成功，宕机了，发生消息丢失；
     *                 消费者手动确认模式。只要我们没有明确告诉MQ，货物被签收，没有ACK,
     *                      消息就一直是unacked状态。即使consumer宕机，消息不会丢失，会重新变为ready，下一次有新的consumer连接进来就发送给他
     *            2、如何签收：
     *            spring.rabbitmq.listener.simple.acknowledge-mode=manual
     *                channel.basicAck(long deliveryTag, boolean multiple);//签收；业务成功完成就应该签收
     *                channel.basicNack(long deliveryTag, boolean multiple, boolean requeue);//拒签；业务失败
     *                channel.basicReject(long deliveryTag, boolean requeue);
     *
     *
     */
    // @PostConstruct  //MyRabbitConfig对象创建完成以后，执行这个方法
    public void initRabbitTemplate() {

        /**
         * @FunctionalInterface
         *     public interface ConfirmCallback {
         *         void confirm(@Nullable CorrelationData var1, boolean var2, @Nullable String var3);
         *     }
         *
         * public void confirm(@NonNull CorrelationData correlationData, boolean ack, @Nullable String cause) {
         *
         * 1、只要消息抵达Broker就ack=true
             * correlationData：当前消息的唯一关联数据(这个是消息的唯一id)
             * ack：消息是否成功收到
             * cause：失败的原因（成功返回null）
         */
        //设置确认回调
        rabbitTemplate.setConfirmCallback((correlationData,ack,cause) -> {
            System.out.println("confirm...correlationData["+correlationData+"]==>ack:["+ack+"]==>cause:["+cause+"]");
        });


        /**
         *
         * public void returnedMessage(Message message, int replyCode, String replyText, String exchange, String routingKey) {
         *
         * 只要消息没有投递给指定的队列，就触发这个失败回调（成功发送对应队列不回调）
         * message：投递失败的消息详细信息
         * replyCode：回复的状态码
         * replyText：回复的文本内容
         * exchange：当时这个消息发给哪个交换机
         * routingKey：当时这个消息用哪个路由键
         */
        rabbitTemplate.setReturnCallback((message,replyCode,replyText,exchange,routingKey) -> {
            System.out.println("Fail Message["+message+"]==>replyCode["+replyCode+"]" +
                    "==>replyText["+replyText+"]==>exchange["+exchange+"]==>routingKey["+routingKey+"]");
        });
    }
}
