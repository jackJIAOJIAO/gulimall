package com.yxj.gulimall.product.web;

import com.yxj.gulimall.product.entity.CategoryEntity;
import com.yxj.gulimall.product.service.CategoryService;
import com.yxj.gulimall.product.vo.Catelog2Vo;
import org.redisson.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.ws.rs.Path;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
/**
 * @author yaoxinjia
 */
@Controller
public class IndexController {

    @Autowired
    CategoryService categoryService;
    // 1 查询所有的1级分类

    @Autowired
    RedissonClient redisson;

    @Autowired
    StringRedisTemplate redisTemplate;


    @GetMapping({"/", "/index.html"})
    public String indexPage(Model model) {
        List<CategoryEntity> categoryEntities = categoryService.getLevel1Categorys();
        model.addAttribute("categorys", categoryEntities);
        return "index";
    }

    @ResponseBody
    @GetMapping("/index/catalog.json")
    public Map<String, List<Catelog2Vo>> getCatalogJson() {

        Map<String, List<Catelog2Vo>> catalogJson = categoryService.getCatalogJson();
        return catalogJson;

    }

    @ResponseBody
    @GetMapping("/hello")
    public String hello() {
        //1 获取一把锁，只要锁的名字一样，就是同一把锁
        RLock lock = redisson.getLock("my-lock");
        // 2 加锁
        lock.lock(); // ******阻塞式等待，默认加的锁都是30s，/不获取锁之前会一直等待
        // ******锁的自动续期，如果业务超长，运行期间自动给锁续上新的30s。不用担心业务时间长，锁自动过期被删除
        // ******加锁的业务只要运行完成，就不会给当前锁续期，即使不手动解锁，锁默认在30s以后自动删除，/自动过期解锁（没有死锁问题）
        lock.lock(10, TimeUnit.SECONDS); //10s自动解锁，自动解锁时间一定要大于业务的执行时间，否则会报删除别的锁的错
//        lock.lock(10, TimeUnit.SECONDS); // ********手动设置过期时间的话，锁时间到了之后，不会自动续期
        try {
            System.out.println("加锁成功，执行业务" + Thread.currentThread().getId());
            Thread.sleep(30000);
        } catch (Exception e) {

        }finally {
            // 3 解锁
            System.out.println("释放锁" + Thread.currentThread().getId());
            lock.unlock();
        }
        return "hello";
    }

    /**
     * 1、改数据加写锁，读数据加读锁
     * 2、保证一定能读到最新数据，只要数据修改，就必须加写锁（排他锁/互斥锁），写锁释放后读锁（共享锁）才能得到
     * 写锁没释放读锁就必须等待
     *
     * 读 + 读：相当于无锁，只会在redis中记录好，所有当前的读锁，他们都会同时加锁成功-共享锁
     * 写 + 读：等待写锁释放
     * 写 + 写：阻塞方式-等待
     * 读 + 写：有读锁，写也需要等待
     * 只要有写的存在，都必须等待
     * @return
     */
    @GetMapping("/write")
    @ResponseBody
    public String writeValue() {
        RReadWriteLock lock = redisson.getReadWriteLock("rw-lock");
        String s = "";
        RLock rLock = lock.writeLock();
        try {
            rLock.lock();
            System.out.println("nihao");
            s = UUID.randomUUID().toString();
            Thread.sleep(30000);
            redisTemplate.opsForValue().set("writeValue", s);
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            rLock.unlock();//死锁
        }
        return s;
    }

    @GetMapping("/read")
    @ResponseBody
    public String readValue() {
        RReadWriteLock lock = redisson.getReadWriteLock("rw-lock");
        String s = "";
        RLock rLock = lock.readLock();
        try {
            rLock.lock();
            s = redisTemplate.opsForValue().get("writeValue");
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            rLock.unlock();
        }
        return s;
    }

    /**
     *车库停车，占车位/开走     信号量
     * @return
     * @throws InterruptedException
     */
    @GetMapping("/park")  //做一个限流工作
    @ResponseBody
    public String park() throws InterruptedException {
        RSemaphore park = redisson.getSemaphore("park");
//        park.acquire();//获取一个信号量，获取一个值，占一个车位，阻塞式等待，会一直等释放
        boolean b = park.tryAcquire();//看是否有车位，是否有值
        if(b){      //有值
            //执行任务
        }else{
            return "error";//当前流量过大，请稍等
        }
        return "ok=>"+b;
    }

    @GetMapping("/go")
    @ResponseBody
    public String go() throws InterruptedException {
        RSemaphore park = redisson.getSemaphore("park");
        //Semaphore
        park.release();//释放一个车位
        return "ok";
    }

    /**
     * 放假 锁门
     * 假如5个班全部走完，才可以锁门   闭锁
     */
    @GetMapping("/lockDoor")
    @ResponseBody
    public String lockDoor() throws InterruptedException {
        RCountDownLatch door = redisson.getCountDownLatch("door");
        door.trySetCount(5);

        door.await();//等待闭锁都完成

        return "放假了。。。。";
    }

    @GetMapping("/gogogo/{id}")
    @ResponseBody
    public String gogogo(@PathVariable("id") Long id) throws InterruptedException {
        RCountDownLatch door = redisson.getCountDownLatch("door");
        door.countDown();//计数减一

   //     CountDownLatch  JUC,用法一样

        return id+"班的人都走了。。。。";
    }


}
