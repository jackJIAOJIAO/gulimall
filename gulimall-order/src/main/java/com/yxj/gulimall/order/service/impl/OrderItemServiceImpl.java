package com.yxj.gulimall.order.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rabbitmq.client.Channel;
import com.yxj.gulimall.common.utils.PageUtils;
import com.yxj.gulimall.common.utils.Query;
import com.yxj.gulimall.order.dao.OrderItemDao;
import com.yxj.gulimall.order.entity.OrderEntity;
import com.yxj.gulimall.order.entity.OrderItemEntity;
import com.yxj.gulimall.order.entity.OrderReturnReasonEntity;
import com.yxj.gulimall.order.service.OrderItemService;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.IOException;
import java.util.Map;

/**
 * @author yaoxinjia
 * @email 894548575@qq.com
 */
@RabbitListener(queues = {"hello-java-queue"})
@Service("orderItemService")
public class OrderItemServiceImpl extends ServiceImpl<OrderItemDao, OrderItemEntity> implements OrderItemService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<OrderItemEntity> page = this.page(
                new Query<OrderItemEntity>().getPage(params),
                new QueryWrapper<OrderItemEntity>()
        );

        return new PageUtils(page);
    }


    /**
     * queues：声明需要监听的队列
     * channel：当前传输数据的通道
     *
     ****** Message  原生消息类型
     *   public Message(byte[] body, MessageProperties messageProperties) {
     *         this.body = body;
     *         this.messageProperties = messageProperties;
     *     }
     *
     * *****参数可以写一下类型
     *  1、Message原生消息详细信息：头+体
     *  2、T<发送的消息的类型>，直接获取OrderReturnReasonEntity content
     *  3、Channel channel：当前传输数据的通道
     *
     * ******监听接收消息：使用   @RabbitListener(queues) + @RabbitHandler          (必须加上@EnableRabbit)
     *  @RabbitListener(queues = "order.release.order.queue")   +  @RabbitHandler//在对应的方法上执行具体接收操作，重载区分各种不同的消息
     *  @RabbitListener标在 类+方法上   @RabbitHandler标在 方法上  //重载处理各种不同的消息类型
     *
     *
     *
     *  ********* Queue 可以很多人来监听。只要收到消息，队列删除消息，而且只能有一个客户端收到此消息
     *  *     场景：
     *  *         1）、订单服务启动多个；同一个消息（队列中），只能有一个客户端收到 / 竞争关系
     *  *         2）、只有一个消息完全处理完，方法运行结束，我们就可以接收到下一个消息
     *            3）、一个生产者生产消息，多个消费者同时收到同一个消息（发布-订阅）---topicExchange (条件广播) / FanoutExchange（无条件广播）
     *  *                                                         （DirectExchange 单播-点对点 HeaderExchange）
     */
    //@RabbitListener(queues = {"hello-java-queue"})
    @RabbitHandler
    public void revieveMessage(Message message,          //1、原生消息详细信息：头+体
                               OrderReturnReasonEntity content,  //  2、T<发送的消息的类型>，直接获取OrderReturnReasonEntity content
                               Channel channel){    //3、Channel channel：当前传输数据的通道
        //拿到主体内容
        byte[] body = message.getBody();
        //拿到的消息头属性信息
        MessageProperties messageProperties = message.getMessageProperties();
        System.out.println("接受到的消息...内容" + message + "===内容：" + content);
        System.out.println("消息处理完成。。。。。。。");
        /**
         * public void basicAck(long deliveryTag, boolean multiple)
         * deliveryTag： Channel通道内按顺序自增
         * multiple： 是否批量确认
         */
        //Channel通道内按顺序自增
        long deliveryTag = message.getMessageProperties().getDeliveryTag();
        try {
            if(deliveryTag%2==0) {
                //收获
                channel.basicAck(deliveryTag, false);//签收消息，确认收到，非批量模式
            }else {
                //退货
                /**
                 * public void basicNack(long deliveryTag, boolean multiple, boolean requeue) throws IOException
                 * requeue：拒绝后是否重新入队，true：重新入队；false：消息丢弃
                 *
                 * public void basicReject(long deliveryTag, boolean requeue) throws IOException {
                 * 不能批量操作
                 */
                channel.basicNack(deliveryTag,false,true);
                channel.basicReject(deliveryTag,true);
                System.out.println("没有签收货物。。。。。。。。。。"+deliveryTag);
            }
        }catch (Exception e){
            //网络中断
        }

    }

    @RabbitHandler //重载处理各种不同的消息类型 OrderReturnReasonEntity / OrderEntity
    public void revieveMessage2(OrderEntity content){

        System.out.println("接受到的消息...内容"  + content);

    }

}